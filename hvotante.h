#ifndef HVOTANTE
#define HVOTANTE
// CODIGO EXEMPLO
 
#define MAX 60
 
/* N� de liga��o */
struct noVotante {
    struct noVotante * prox;
    struct noVotante * ant;
    void * dados;
};
typedef struct noVotante NOVOTANTE;
typedef NOVOTANTE * PNOVOTANTE;
typedef NOVOTANTE ** PPNOVOTANTE;
 
/* Elemento de dados */
struct votante {
    int numero;
    char nome[MAX];
};
typedef struct votante VOTANTE;
typedef VOTANTE * PVOTANTE;
 
/* Cria votante */
PVOTANTE criaVotante() {
        PVOTANTE novo;
        novo = (PVOTANTE)malloc(sizeof(VOTANTE));
        printf("Numero: ");
        scanf("%d", &novo->numero);
    /* Apaga resto da linha */
        scanf("%*[^\n]"); scanf("%*c");
        printf("Nome: ");
        fflush (stdout);
        fgets(novo->nome, MAX, stdin);
    /* Elimina '\n' no fim da string */
        novo->nome[strlen(novo->nome)-1] = '\0';
        return novo;
}
 
/* Inserir no in�cio */
void insereVotanteInicio(PPNOVOTANTE cabeca, void * dados) {
    PNOVOTANTE novo;
    novo = (PNOVOTANTE)malloc(sizeof(NOVOTANTE));
    if(*cabeca) (*cabeca)->ant = novo;
    novo->prox = *cabeca;
    novo->ant = NULL;
    *cabeca = novo;
    novo->dados = dados;
}
 
/* Inserir no fim */
void insereVotanteFim(PPNOVOTANTE cabeca, void * dados) {
    PNOVOTANTE ptr, novo;
    if(!*cabeca)
        insereVotanteInicio(cabeca, dados);
    else {
        for(ptr=*cabeca; ptr->prox; ptr=ptr->prox);
        novo = (PNOVOTANTE)malloc(sizeof(NOVOTANTE));
        ptr->prox = novo;
        novo->prox = NULL;
        novo->ant = ptr;
        novo->dados = dados;
 
    }
}
 
void imprimeVotante2(PNOVOTANTE cabeca, void (*imprimeDados)()) {
    PNOVOTANTE ptr;
    for(ptr=cabeca; ptr; ptr=ptr->prox) {
        (*imprimeDados)(ptr->dados);
    }
}
 
void imprimeVotante(PVOTANTE p) {
    printf("%d - %s\n", p->numero, p->nome);
}
 
/* Fun��o gen�rica de pesquisa */
void* pesquisaVotante(PNOVOTANTE cabeca, int (*compara)(), void* valor) {
    PNOVOTANTE ptr;
    for(ptr=cabeca; ptr; ptr=ptr->prox)
        if((*compara)(ptr->dados, valor))
            return ptr->dados;
    return NULL;
}
 
/* Fun��es auxiliares de pesquisa */
int cmpNumVotante(PVOTANTE a, int* num) {
    return a->numero == *num;
}
 
int cmpNomeVotante(PVOTANTE a, char* nome) {
    return strcmp(a->nome, nome) == 0;
}
 
/* Quicksort - fun��es auxiliares */
int nomeCresVotante(PVOTANTE a, PVOTANTE b) {
    return strcmp(a->nome, b->nome) > 0;
}
 
int nomeDecrVotante(PVOTANTE a, PVOTANTE b) {
    return strcmp(a->nome, b->nome) < 0;
}
 
int numCresVotante(PVOTANTE a, PVOTANTE b) {
    return a->numero > b->numero;
}
 
int numDecrVotante(PVOTANTE a, PVOTANTE b) {
    return a->numero < b->numero;
}
 
PNOVOTANTE concatenaVotante(PNOVOTANTE lista1, PNOVOTANTE lista2) {
    PNOVOTANTE ptr;
    if(!lista1) return lista2;
    if(!lista2) return lista1;
    for (ptr=lista1; ptr->prox; ptr=ptr->prox);
    ptr->prox = lista2;
    lista2->ant = ptr;
    return lista1;
}
 
void* popVotante (PPNOVOTANTE cabeca) {
    void* ptr;
    PNOVOTANTE c;
    if(!*cabeca) return NULL;
    ptr = (*cabeca)->dados;
    c = *cabeca;
    *cabeca = (*cabeca)->prox;
    c->prox = NULL;
    if(*cabeca) (*cabeca)->ant = NULL;
    free(c);
    return ptr;
}
 
/* Quicksort - fun��o gen�rica */
PNOVOTANTE quicksortVotante(PNOVOTANTE cabeca, int (*compara)()) {
    PNOVOTANTE menores=NULL, maiores=NULL, lpivot = NULL;
    void *pivot, *ptr;
    if(!(pivot = popVotante(&cabeca))) return NULL;
    while(ptr = popVotante(&cabeca)) {
        if((*compara)(ptr,pivot)) insereVotanteInicio(&maiores, ptr);
        else insereVotanteInicio(&menores, ptr);
    }
    insereVotanteInicio(&lpivot, pivot);
    return concatenaVotante(concatenaVotante(quicksortVotante(menores,compara),
                    lpivot), quicksortVotante(maiores,compara));
}
 
int removerVotante(PPNOVOTANTE cabeca, int (*compara)(), void* valor) {
    PNOVOTANTE ptr;
    for(ptr=*cabeca; ptr; ptr=ptr->prox)
        if((*compara)(ptr->dados, valor)){
            if(ptr->ant) ptr->ant->prox = ptr->prox;
            else *cabeca = ptr->prox;
            if(ptr->prox) ptr->prox->ant = ptr->ant;
            free(ptr->dados);
            free(ptr);
            return 1;
        }
    return 0;
}
 
 
int mvotante() {
    PNOVOTANTE votantes=NULL;
    PVOTANTE ptr;
    int num, n;
    char nome[MAX];
    int opcao;
    do {
        printf("\nVotantes\n");
        printf("\n1 - Inserir votante no inicio");
        printf("\n2 - Inserir votante no fim");
        printf("\n3 - Imprimir lista de votantes");
        printf("\n4 - Pesquisar por numero");
        printf("\n5 - Pesquisar por nome");
        printf("\n6 - Ordenar por numero, ordem crescente");
        printf("\n7 - Ordenar por nome, ordem crescente");
        printf("\n8 - Ordenar por numero, ordem decrescente");
        printf("\n9 - Ordenar por nome, ordem decrescente");
        printf("\na - Remover por numero");
        printf("\nb - Remover por nome");
        printf("\n0 - Sair");
        printf("\nInsira a opcao: ");
        opcao = getchar();
        scanf("%*[^\n]"); scanf("%*c");
        switch(opcao) {
            case '1':
                ptr = criaVotante();
                insereVotanteInicio(&votantes,ptr);
                break;
            case '2':
                ptr = criaVotante();
                insereVotanteFim(&votantes,ptr);
                break;
            case '3':
                imprimeVotante2(votantes,imprimeVotante);
                break;
            case '4':
                printf("Numero: ");
                    scanf("%d", &num);
                    scanf("%*[^\n]"); scanf("%*c");
                ptr = pesquisaVotante(votantes, cmpNumVotante, &num);
                if(ptr) imprimeVotante(ptr);
                else printf("Votante inexistente");
                break;
            case '5':
                printf("Nome: ");
                    fgets(nome, MAX, stdin);
                    nome[strlen(nome)-1] = '\0';
                ptr = pesquisaVotante(votantes, cmpNomeVotante, nome);
                if(ptr) imprimeVotante(ptr);
                else printf("Votante inexistente");
                break;
            case '6':
                votantes = quicksortVotante(votantes,numCresVotante);
                break;
            case '7':
                votantes = quicksortVotante(votantes,nomeCresVotante);
                break;
            case '8':
                votantes = quicksortVotante(votantes,numDecrVotante);
                break;
            case '9':
                votantes = quicksortVotante(votantes,nomeDecrVotante);
                break;
            case 'a':
                printf("Numero: ");
                    scanf("%d", &num);
                    scanf("%*[^\n]"); scanf("%*c");
                n = removerVotante(&votantes, cmpNumVotante, &num);
                if(n) printf("Votante removida");
                else printf("Votante inexistente");
                break;
            case 'b':
                printf("Nome: ");
                    fgets(nome, MAX, stdin);
                    nome[strlen(nome)-1] = '\0';
                n = removerVotante(&votantes, cmpNomeVotante, nome);
                if(n) printf("Votante removida");
                else printf("Votante inexistente");
                break;
        }
    } while(opcao != '0');
}
#endif