#ifndef HCATEGORIA
#define HCATEGORIA
// CODIGO EXEMPLO

#define MAX 60

/* Ficheiro para armazenar dados*/
FILE *fpCategoria;

/* N� de liga��o */
struct noCategoria {
    struct noCategoria * prox;
    struct noCategoria * ant;
    void * dados;
};
typedef struct noCategoria NOCATEGORIA;
typedef NOCATEGORIA * PNOCATEGORIA;
typedef NOCATEGORIA ** PPNOCATEGORIA;
 
/* Elemento de dados */
struct categoria {
    int numero;
    char nome[MAX];
};
typedef struct categoria CATEGORIA;
typedef CATEGORIA * PCATEGORIA;
 
/* Cria categoria */
PCATEGORIA criaCategoria() {
        PCATEGORIA novo;
        novo = (PCATEGORIA)malloc(sizeof(CATEGORIA));
        printf("Numero: ");
        scanf("%d", &novo->numero);
    /* Apaga resto da linha */
        scanf("%*[^\n]"); scanf("%*c");
        printf("Nome: ");
        fflush (stdout);
        fgets(novo->nome, MAX, stdin);
    /* Elimina '\n' no fim da string */
        novo->nome[strlen(novo->nome)-1] = '\0';
        return novo;
}

/* Inserir no in�cio */
void insereCategoriaInicio(PPNOCATEGORIA cabeca, void * dados) {
    PNOCATEGORIA novo;
    novo = (PNOCATEGORIA)malloc(sizeof(NOCATEGORIA));
    if(*cabeca) (*cabeca)->ant = novo;
    novo->prox = *cabeca;
    novo->ant = NULL;
    *cabeca = novo;
    novo->dados = dados;
}
 
/* Inserir no fim */
void insereCategoriaFim(PPNOCATEGORIA cabeca, void * dados) {
    PNOCATEGORIA ptr, novo;
    if(!*cabeca)
        insereCategoriaInicio(cabeca, dados);
    else {
        for(ptr=*cabeca; ptr->prox; ptr=ptr->prox);
        novo = (PNOCATEGORIA)malloc(sizeof(NOCATEGORIA));
        ptr->prox = novo;
        novo->prox = NULL;
        novo->ant = ptr;
        novo->dados = dados;
 
    }
}

/*Escreve dados no ficheiro*/
void insereCategoria(void * dados) {
    PCATEGORIA p;
    p = dados;
    fpCategoria = fopen("categoria.dat", "ab");
    fwrite(p, sizeof(NOCATEGORIA), 1, fpCategoria);
    fclose(fpCategoria);
}

void leCategoria() {
    PCATEGORIA p2;
    p2 = (PCATEGORIA)malloc(sizeof(CATEGORIA));
    fpCategoria = fopen("categoria.dat", "rb");
    if (fpCategoria != NULL) {
        long fsize ;
        fseek(fpCategoria, 0, SEEK_END);
        fsize =ftell(fpCategoria);
        printf("total size is %ld bytes\n",fsize);
        
        int counter=0;
        long size=0;
        while(size<fsize){
            fseek(fpCategoria,sizeof(NOCATEGORIA)*counter,SEEK_SET);
	        fread(p2,sizeof(NOCATEGORIA),1,fpCategoria);
	        printf("%d - %s\n", p2->numero, p2->nome);
	        counter++;
	        size=sizeof(NOCATEGORIA)*counter;
        }
	    fclose(fpCategoria);
    }
}

void imprimeCategoria2(PNOCATEGORIA cabeca, void (*imprimeDados)()) {
    PNOCATEGORIA ptr;
    for(ptr=cabeca; ptr; ptr=ptr->prox) {
        (*imprimeDados)(ptr->dados);
    }
}
 
void imprimeCategoria(PCATEGORIA p) {
    printf("%d - %s\n", p->numero, p->nome);
}
 
/* Fun��o gen�rica de pesquisa */
void* pesquisaCategoria(PNOCATEGORIA cabeca, int (*compara)(), void* valor) {
    PNOCATEGORIA ptr;
    for(ptr=cabeca; ptr; ptr=ptr->prox)
        if((*compara)(ptr->dados, valor))
            return ptr->dados;
    return NULL;
}
 
/* Fun��es auxiliares de pesquisa */
int cmpNumCategoria(PCATEGORIA a, int* num) {
    return a->numero == *num;
}
 
int cmpNomeCategoria(PCATEGORIA a, char* nome) {
    return strcmp(a->nome, nome) == 0;
}
 
/* Quicksort - fun��es auxiliares */
int nomeCresCategoria(PCATEGORIA a, PCATEGORIA b) {
    return strcmp(a->nome, b->nome) > 0;
}
 
int nomeDecrCategoria(PCATEGORIA a, PCATEGORIA b) {
    return strcmp(a->nome, b->nome) < 0;
}
 
int numCresCategoria(PCATEGORIA a, PCATEGORIA b) {
    return a->numero > b->numero;
}
 
int numDecrCategoria(PCATEGORIA a, PCATEGORIA b) {
    return a->numero < b->numero;
}
 
PNOCATEGORIA concatenaCategoria(PNOCATEGORIA lista1, PNOCATEGORIA lista2) {
    PNOCATEGORIA ptr;
    if(!lista1) return lista2;
    if(!lista2) return lista1;
    for (ptr=lista1; ptr->prox; ptr=ptr->prox);
    ptr->prox = lista2;
    lista2->ant = ptr;
    return lista1;
}
 
void* popCategoria (PPNOCATEGORIA cabeca) {
    void* ptr;
    PNOCATEGORIA c;
    if(!*cabeca) return NULL;
    ptr = (*cabeca)->dados;
    c = *cabeca;
    *cabeca = (*cabeca)->prox;
    c->prox = NULL;
    if(*cabeca) (*cabeca)->ant = NULL;
    free(c);
    return ptr;
}
 
/* Quicksort - fun��o gen�rica */
PNOCATEGORIA quicksortCategoria(PNOCATEGORIA cabeca, int (*compara)()) {
    PNOCATEGORIA menores=NULL, maiores=NULL, lpivot = NULL;
    void *pivot, *ptr;
    if(!(pivot = popCategoria(&cabeca))) return NULL;
    while(ptr = popCategoria(&cabeca)) {
        if((*compara)(ptr,pivot)) insereCategoriaInicio(&maiores, ptr);
        else insereCategoriaInicio(&menores, ptr);
    }
    insereCategoriaInicio(&lpivot, pivot);
    return concatenaCategoria(concatenaCategoria(quicksortCategoria(menores,compara),
                    lpivot), quicksortCategoria(maiores,compara));
}
 
int removerCategoria(PPNOCATEGORIA cabeca, int (*compara)(), void* valor) {
    PNOCATEGORIA ptr;
    for(ptr=*cabeca; ptr; ptr=ptr->prox)
        if((*compara)(ptr->dados, valor)){
            if(ptr->ant) ptr->ant->prox = ptr->prox;
            else *cabeca = ptr->prox;
            if(ptr->prox) ptr->prox->ant = ptr->ant;
            free(ptr->dados);
            free(ptr);
            return 1;
        }
    return 0;
}
 
 
int mcategoria() {
    PNOCATEGORIA categorias=NULL;
    PCATEGORIA ptr;
    int num, n;
    char nome[MAX];
    int opcao;
    do {
        printf("\nCategorias\n");
        printf("\n1 - Inserir categoria");
        //printf("\n1 - Inserir categoria no inicio");
        //printf("\n2 - Inserir categoria no fim");
        printf("\n3 - Imprimir lista de categorias");
        printf("\n4 - Pesquisar por numero");
        printf("\n5 - Pesquisar por nome");
        printf("\n6 - Ordenar por numero, ordem crescente");
        printf("\n7 - Ordenar por nome, ordem crescente");
        printf("\n8 - Ordenar por numero, ordem decrescente");
        printf("\n9 - Ordenar por nome, ordem decrescente");
        printf("\na - Remover por numero");
        printf("\nb - Remover por nome");
        printf("\n0 - Sair");
        printf("\nInsira a opcao: ");
        opcao = getchar();
        scanf("%*[^\n]"); scanf("%*c");
        switch(opcao) {
            case '1':
                ptr = criaCategoria();
                //insereCategoriaInicio(&categorias,ptr);
                insereCategoria(ptr);
                break;
            case '2':
                ptr = criaCategoria();
                insereCategoriaFim(&categorias,ptr);
                break;
            case '3':
                leCategoria();
                imprimeCategoria2(categorias,imprimeCategoria);
                break;
            case '4':
                printf("Numero: ");
                    scanf("%d", &num);
                    scanf("%*[^\n]"); scanf("%*c");
                ptr = pesquisaCategoria(categorias, cmpNumCategoria, &num);
                if(ptr) imprimeCategoria(ptr);
                else printf("Categoria inexistente");
                break;
            case '5':
                printf("Nome: ");
                    fgets(nome, MAX, stdin);
                    nome[strlen(nome)-1] = '\0';
                ptr = pesquisaCategoria(categorias, cmpNomeCategoria, nome);
                if(ptr) imprimeCategoria(ptr);
                else printf("Categoria inexistente");
                break;
            case '6':
                categorias = quicksortCategoria(categorias,numCresCategoria);
                break;
            case '7':
                categorias = quicksortCategoria(categorias,nomeCresCategoria);
                break;
            case '8':
                categorias = quicksortCategoria(categorias,numDecrCategoria);
                break;
            case '9':
                categorias = quicksortCategoria(categorias,nomeDecrCategoria);
                break;
            case 'a':
                printf("Numero: ");
                    scanf("%d", &num);
                    scanf("%*[^\n]"); scanf("%*c");
                n = removerCategoria(&categorias, cmpNumCategoria, &num);
                if(n) printf("Categoria removida");
                else printf("Categoria inexistente");
                break;
            case 'b':
                printf("Nome: ");
                    fgets(nome, MAX, stdin);
                    nome[strlen(nome)-1] = '\0';
                n = removerCategoria(&categorias, cmpNomeCategoria, nome);
                if(n) printf("Categoria removida");
                else printf("Categoria inexistente");
                break;
        }
    } while(opcao != '0');
    
}
#endif