#ifndef HCANDIDATO
#define HCANDIDATO
// CODIGO EXEMPLO
 
#define MAX 60
 
/* N� de liga��o */
struct noCandidato {
    struct noCandidato * prox;
    struct noCandidato * ant;
    void * dados;
};
typedef struct noCandidato NOCANDIDATO;
typedef NOCANDIDATO * PNOCANDIDATO;
typedef NOCANDIDATO ** PPNOCANDIDATO;
 
/* Elemento de dados */
struct candidato {
    int numero;
    char nome[MAX];
};
typedef struct candidato CANDIDATO;
typedef CANDIDATO * PCANDIDATO;
 
/* Cria candidato */
PCANDIDATO criaCandidato() {
        PCANDIDATO novo;
        novo = (PCANDIDATO)malloc(sizeof(CANDIDATO));
        printf("Numero: ");
        scanf("%d", &novo->numero);
    /* Apaga resto da linha */
        scanf("%*[^\n]"); scanf("%*c");
        printf("Nome: ");
        fflush (stdout);
        fgets(novo->nome, MAX, stdin);
    /* Elimina '\n' no fim da string */
        novo->nome[strlen(novo->nome)-1] = '\0';
        return novo;
}
 
/* Inserir no in�cio */
void insereCandidatoInicio(PPNOCANDIDATO cabeca, void * dados) {
    PNOCANDIDATO novo;
    novo = (PNOCANDIDATO)malloc(sizeof(NOCANDIDATO));
    if(*cabeca) (*cabeca)->ant = novo;
    novo->prox = *cabeca;
    novo->ant = NULL;
    *cabeca = novo;
    novo->dados = dados;
}
 
/* Inserir no fim */
void insereCandidatoFim(PPNOCANDIDATO cabeca, void * dados) {
    PNOCANDIDATO ptr, novo;
    if(!*cabeca)
        insereCandidatoInicio(cabeca, dados);
    else {
        for(ptr=*cabeca; ptr->prox; ptr=ptr->prox);
        novo = (PNOCANDIDATO)malloc(sizeof(NOCANDIDATO));
        ptr->prox = novo;
        novo->prox = NULL;
        novo->ant = ptr;
        novo->dados = dados;
 
    }
}
 
void imprimeCandidato2(PNOCANDIDATO cabeca, void (*imprimeDados)()) {
    PNOCANDIDATO ptr;
    for(ptr=cabeca; ptr; ptr=ptr->prox) {
        (*imprimeDados)(ptr->dados);
    }
}
 
void imprimeCandidato(PCANDIDATO p) {
    printf("%d - %s\n", p->numero, p->nome);
}
 
/* Fun��o gen�rica de pesquisa */
void* pesquisaCandidato(PNOCANDIDATO cabeca, int (*compara)(), void* valor) {
    PNOCANDIDATO ptr;
    for(ptr=cabeca; ptr; ptr=ptr->prox)
        if((*compara)(ptr->dados, valor))
            return ptr->dados;
    return NULL;
}
 
/* Fun��es auxiliares de pesquisa */
int cmpNumCandidato(PCANDIDATO a, int* num) {
    return a->numero == *num;
}
 
int cmpNomeCandidato(PCANDIDATO a, char* nome) {
    return strcmp(a->nome, nome) == 0;
}
 
/* Quicksort - fun��es auxiliares */
int nomeCresCandidato(PCANDIDATO a, PCANDIDATO b) {
    return strcmp(a->nome, b->nome) > 0;
}
 
int nomeDecrCandidato(PCANDIDATO a, PCANDIDATO b) {
    return strcmp(a->nome, b->nome) < 0;
}
 
int numCresCandidato(PCANDIDATO a, PCANDIDATO b) {
    return a->numero > b->numero;
}
 
int numDecrCandidato(PCANDIDATO a, PCANDIDATO b) {
    return a->numero < b->numero;
}
 
PNOCANDIDATO concatenaCandidato(PNOCANDIDATO lista1, PNOCANDIDATO lista2) {
    PNOCANDIDATO ptr;
    if(!lista1) return lista2;
    if(!lista2) return lista1;
    for (ptr=lista1; ptr->prox; ptr=ptr->prox);
    ptr->prox = lista2;
    lista2->ant = ptr;
    return lista1;
}
 
void* popCandidato (PPNOCANDIDATO cabeca) {
    void* ptr;
    PNOCANDIDATO c;
    if(!*cabeca) return NULL;
    ptr = (*cabeca)->dados;
    c = *cabeca;
    *cabeca = (*cabeca)->prox;
    c->prox = NULL;
    if(*cabeca) (*cabeca)->ant = NULL;
    free(c);
    return ptr;
}
 
/* Quicksort - fun��o gen�rica */
PNOCANDIDATO quicksortCandidato(PNOCANDIDATO cabeca, int (*compara)()) {
    PNOCANDIDATO menores=NULL, maiores=NULL, lpivot = NULL;
    void *pivot, *ptr;
    if(!(pivot = popCandidato(&cabeca))) return NULL;
    while(ptr = popCandidato(&cabeca)) {
        if((*compara)(ptr,pivot)) insereCandidatoInicio(&maiores, ptr);
        else insereCandidatoInicio(&menores, ptr);
    }
    insereCandidatoInicio(&lpivot, pivot);
    return concatenaCandidato(concatenaCandidato(quicksortCandidato(menores,compara),
                    lpivot), quicksortCandidato(maiores,compara));
}
 
int removerCandidato(PPNOCANDIDATO cabeca, int (*compara)(), void* valor) {
    PNOCANDIDATO ptr;
    for(ptr=*cabeca; ptr; ptr=ptr->prox)
        if((*compara)(ptr->dados, valor)){
            if(ptr->ant) ptr->ant->prox = ptr->prox;
            else *cabeca = ptr->prox;
            if(ptr->prox) ptr->prox->ant = ptr->ant;
            free(ptr->dados);
            free(ptr);
            return 1;
        }
    return 0;
}
 
 
int mcandidato() {
    PNOCANDIDATO candidatos=NULL;
    PCANDIDATO ptr;
    int num, n;
    char nome[MAX];
    int opcao;
    do {
        printf("\nCandidatos\n");
        printf("\n1 - Inserir candidato no inicio");
        printf("\n2 - Inserir candidato no fim");
        printf("\n3 - Imprimir lista de candidatos");
        printf("\n4 - Pesquisar por numero");
        printf("\n5 - Pesquisar por nome");
        printf("\n6 - Ordenar por numero, ordem crescente");
        printf("\n7 - Ordenar por nome, ordem crescente");
        printf("\n8 - Ordenar por numero, ordem decrescente");
        printf("\n9 - Ordenar por nome, ordem decrescente");
        printf("\na - Remover por numero");
        printf("\nb - Remover por nome");
        printf("\n0 - Sair");
        printf("\nInsira a opcao: ");
        opcao = getchar();
        scanf("%*[^\n]"); scanf("%*c");
        switch(opcao) {
            case '1':
                ptr = criaCandidato();
                insereCandidatoInicio(&candidatos,ptr);
                break;
            case '2':
                ptr = criaCandidato();
                insereCandidatoFim(&candidatos,ptr);
                break;
            case '3':
                imprimeCandidato2(candidatos,imprimeCandidato);
                break;
            case '4':
                printf("Numero: ");
                    scanf("%d", &num);
                    scanf("%*[^\n]"); scanf("%*c");
                ptr = pesquisaCandidato(candidatos, cmpNumCandidato, &num);
                if(ptr) imprimeCandidato(ptr);
                else printf("Candidato inexistente");
                break;
            case '5':
                printf("Nome: ");
                    fgets(nome, MAX, stdin);
                    nome[strlen(nome)-1] = '\0';
                ptr = pesquisaCandidato(candidatos, cmpNomeCandidato, nome);
                if(ptr) imprimeCandidato(ptr);
                else printf("Candidato inexistente");
                break;
            case '6':
                candidatos = quicksortCandidato(candidatos,numCresCandidato);
                break;
            case '7':
                candidatos = quicksortCandidato(candidatos,nomeCresCandidato);
                break;
            case '8':
                candidatos = quicksortCandidato(candidatos,numDecrCandidato);
                break;
            case '9':
                candidatos = quicksortCandidato(candidatos,nomeDecrCandidato);
                break;
            case 'a':
                printf("Numero: ");
                    scanf("%d", &num);
                    scanf("%*[^\n]"); scanf("%*c");
                n = removerCandidato(&candidatos, cmpNumCandidato, &num);
                if(n) printf("Candidato removida");
                else printf("Candidato inexistente");
                break;
            case 'b':
                printf("Nome: ");
                    fgets(nome, MAX, stdin);
                    nome[strlen(nome)-1] = '\0';
                n = removerCandidato(&candidatos, cmpNomeCandidato, nome);
                if(n) printf("Candidato removida");
                else printf("Candidato inexistente");
                break;
        }
    } while(opcao != '0');
}
#endif