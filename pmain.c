// CODIGO EXEMPLO
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hcategoria.h"
#include "hcandidato.h"
#include "hvotante.h"
#define MAX 60

int main() {
    int num, n;
    char nome[MAX];
    int opcao;
    do {
        printf("\nGestor de Premios (Awards)\n");
        printf("\n1 - Gerir categorias");
        printf("\n2 - Gerir candidatos");
        printf("\n3 - Gerir votantes");
        printf("\n4 - Gerir votos");
        printf("\n0 - Sair");
        printf("\nInsira a opcao: ");
        opcao = getchar();
        scanf("%*[^\n]"); scanf("%*c");
        switch(opcao) {
            case '1':
                mcategoria();
                break;
            case '2':
                mcandidato();
                break;
            case '3':
                mvotante();
                break;
            case '4':
                break;
       }
    } while(opcao != '0');
}